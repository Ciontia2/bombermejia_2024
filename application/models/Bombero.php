<?php
class Bombero extends CI_Model {

  public function __construct() {
      parent::__construct();
  }



  function getAll()
  {
    $listMovie=
    $this->db->get("contador");
    if($listMovie->num_rows()>0){
        return $listMovie->result();
    }else{
        return false;
    }
  }
  // *********************** ACTIVIDAD 3 ****************************
  // FUNCION 1: actividad 3
  function getByVisit20()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2020
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }


  function getByVisit21()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2021
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }

  function getByVisit22()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2022
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }

  function getByVisit23()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2023
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }
  //INDICADOR 1: actividad 3
  function getByTotalVisit20()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2020;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 2: actividad 3
  function getByTotalVisit21()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2021;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 3: actividad 3
  function getByTotalVisit22()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2022;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 4: actividad 3
  function getByTotalVisit23()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2023;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }
  // *********************** FIN ACTIVIDAD 3 ****************************


  // *********************** ACTIVIDAD 4 ********************************
  //LIZ

  public function getTotalNotificaciones()
  {
      $sql = "SELECT SUM(total_notificaciones) AS suma_total FROM (SELECT COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol) AS subquery;";
      $result = $this->db->query($sql);

      if ($result->num_rows() > 0) {
          $row = $result->row();
          return $row->suma_total;
      } else {
          return 0;
      }
  }
  public function getTotalNotificacionesTop10()
  {
      $sql = "SELECT SUM(total_notificaciones) AS suma_total FROM (SELECT COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol ORDER BY total_notificaciones DESC LIMIT 10) AS subquery;";
      $result = $this->db->query($sql);

      if ($result->num_rows() > 0) {
          $row = $result->row();
          return $row->suma_total;
      } else {
          return 0;
      }
  }


  function getByTotalNotification($order,$limit){
        $sql="select s.codigo_sol, COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol ORDER BY total_notificaciones $order LIMIT $limit  ;";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result();
          }else {
            return 0;
          }
        }


        public function getTotalEstados()
       {
           $sql = "select COUNT(DISTINCT estado_sol) AS total_estados FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol;";
           $result = $this->db->query($sql);

           if ($result->num_rows() > 0) {
               $row = $result->row();
               return $row->total_estados;
           } else {
               return 0;
           }
       }
        function getByTotalState($order){
              $sql="select estado_sol, COUNT(n.codigo_not) AS count_notificaciones, (COUNT(n.codigo_not) / (SELECT COUNT(codigo_not) FROM notificacion)) * 100 AS porcentaje FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY estado_sol $order ;";
              $result=$this->db->query($sql);
              if($result->num_rows()>0){
                  return $result->result();
                }else {
                  return 0;
                }
              }

              public function getTotalActividades()
              {
                  $sql = "select COUNT(n.codigo_not) AS total_notificaciones
                    FROM solicitud_permiso s
                    LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol;";
                  $result = $this->db->query($sql);

                  if ($result->num_rows() > 0) {
                      $row = $result->row();
                      return $row->total_notificaciones;
                  } else {
                      return 0;
                  }
              }
              function getByTotalActividad($order,$limit){
                    $sql="select s.actividad_sol, YEAR(n.fecha_not) AS anio, COUNT(n.codigo_not) AS count_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.actividad_sol, anio ORDER BY count_notificaciones $order LIMIT $limit ;";
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0){
                        return $result->result();
                      }else {
                        return 0;
                      }
                    }

                                function getTotalSolicitudes() {
                    $sql = "select COUNT(codigo_sol) AS total_solicitudes FROM solicitud_permiso;";
                    $result = $this->db->query($sql);

                    if ($result->num_rows() > 0) {
                        $row = $result->row();
                        return $row->total_solicitudes;
                    } else {
                        return 0;
                    }
                }

                    function getByTotalMes($order){
                          $sql="select anio, COUNT(codigo_sol) AS count_solicitudes, (COUNT(codigo_sol) * 100.0 / SUM(COUNT(codigo_sol)) OVER ()) AS porcentaje FROM ( SELECT YEAR(fecha_sol) AS anio, codigo_sol FROM solicitud_permiso ) AS subquery GROUP BY anio ORDER BY anio $order; ";
                          $result=$this->db->query($sql);
                          if($result->num_rows()>0){
                              return $result->result();
                            }else {
                              return 0;
                            }
                          }
  // *********************** FIN ACTIVIDAD 4 ****************************


  // *********************** ACTIVIDAD 5 ********************************
  //CINTIA
  // *********************** FIN ACTIVIDAD 5 ****************************

  
  
  // *********************** ACTIVIDAD 6 ********************************
  //Funcion 6.1: total de notificaciones y solicitudes que tiene cada Usuario
  public function getDataForKPI6()
  {
      $sql = "SELECT u.nombre_usu AS NombreUsuario, COUNT(sp.codigo_sol) AS TotalSolicitudes, COUNT(n.codigo_not) AS TotalNotificaciones
      FROM usuario u
      LEFT JOIN solicitud_permiso sp ON u.codigo_usu = sp.codigo_usu
      LEFT JOIN notificacion n ON sp.codigo_sol = n.codigo_sol
      GROUP BY u.nombre_usu;";
  
      $result = $this->db->query($sql);
  
      if ($result->num_rows() > 0) {
          return $result->result(); // Devolver todos los resultados
      } else {
          return 0; 
      }
  }
  //Indicador 6.1: total de notificaciones 
  public function getByTotalNotiForKPI6()
  {
      $sql = "SELECT COUNT(codigo_not) AS TotalNotificaciones
      FROM notificacion;";

      $result = $this->db->query($sql);
      if ($result->num_rows() > 0) {
        return $result->row()->TotalNotificaciones; //retorna un valor o 1 registro
      } else {
          return 0; 
      }
     
  }
  //Indicador 6.2: total de solicitudes
  public function getByTotalSoliForKPI6()
  {
      $sql = "SELECT COUNT(codigo_sol) AS TotalSolicitudes
      FROM solicitud_permiso;";

      $result = $this->db->query($sql);
      if ($result->num_rows() > 0) {
        return $result->row()->TotalSolicitudes; //retorna un valor o 1 registro
      } else {
          return 0; 
      }
      
  }
  //Funcion 6.2: Cantidad de solicitudes aprobadas y rechazadas
  public function getDataForKPI_62()
  {
      $sql = "SELECT 
      u.nombre_usu AS NombreUsuario, 
      COUNT(CASE WHEN YEAR(sp.fecha_sol) = 2022 THEN sp.codigo_sol END) AS TotalSolicitudes2022, 
      COUNT(CASE WHEN YEAR(n.fecha_not) = 2022 THEN n.codigo_not END) AS TotalNotificaciones2022
       FROM 
           usuario u
       INNER JOIN 
           solicitud_permiso sp ON u.codigo_usu = sp.codigo_usu
       INNER JOIN 
           notificacion n ON sp.codigo_sol = n.codigo_sol
       WHERE 
           YEAR(sp.fecha_sol) = 2022 OR YEAR(n.fecha_not) = 2022
       GROUP BY 
           u.nombre_usu;";
  
      $result = $this->db->query($sql);
  
      if ($result->num_rows() > 0) {
          return $result->result(); // Devolver todos los resultados
      } else {
          return 0; 
      }
  }
  //Indicador 6.2.1: total de notificaciones  ????? 6 cambiar KPI
 
   //Indicador 6.2.2: total de solicitudes 
   public function getByTotalSoliForKPI7()
   {
       $sql = "SELECT COUNT(codigo_sol) AS TotalSolicitudesAprobadas
       FROM solicitud_permiso
       WHERE estado_sol = 'APROBADO';";
 
       $result = $this->db->query($sql);
       if ($result->num_rows() > 0) {
         return $result->row()->TotalSolicitudesAprobadas; //retorna un valor o 1 registro
       } else {
           return 0; 
       }
     
   }
   //Funcion 6.3 obtener todos de notificiaciones y solicitudes por año 2023
   public function getDataForKPI_63()
   {
       $sql = "SELECT 
       u.nombre_usu AS NombreUsuario, 
       COUNT(CASE WHEN YEAR(sp.fecha_sol) = 2023 THEN sp.codigo_sol END) AS TotalSolicitudes2023, 
       COUNT(CASE WHEN YEAR(n.fecha_not) = 2023 THEN n.codigo_not END) AS TotalNotificaciones2023
        FROM 
            usuario u
        INNER JOIN 
            solicitud_permiso sp ON u.codigo_usu = sp.codigo_usu
        INNER JOIN 
            notificacion n ON sp.codigo_sol = n.codigo_sol
        WHERE 
            YEAR(sp.fecha_sol) = 2023 OR YEAR(n.fecha_not) = 2023
        GROUP BY 
            u.nombre_usu;
        ";
   
       $result = $this->db->query($sql);
   
       if ($result->num_rows() > 0) {
           return $result->result(); // Devolver todos los resultados
       } else {
           return 0; 
       }
   }
  // Indicador 6.3: total de soliciitudes en 2023
  
   //Funcion 6.4 
   public function getDataForKPI_64()
   {
       $sql = "SELECT u.nombre_usu AS NombreUsuario, sp.actividad_sol AS Actividad, 
       COUNT(DISTINCT sp.codigo_sol) AS TotalSolicitudes, 
       COUNT(DISTINCT n.codigo_not) AS TotalNotificaciones 
      FROM solicitud_permiso sp 
      LEFT JOIN notificacion n ON sp.codigo_sol = n.codigo_sol 
      LEFT JOIN usuario u ON sp.codigo_usu = u.codigo_usu 
      WHERE sp.actividad_sol IN ('Secretaria Bomberos', 'Comercio1', 'Comercio2', 
                                'Talleres/Lubricadoras/Lavadoras', 'Industrias', 
                                'Restaurantes', 'Ferreterias', 'Financieras', 'Farmacias') 
      AND u.nombre_usu IS NOT NULL -- Filtrar filas donde nombre_usu no sea nulo
      GROUP BY u.nombre_usu, sp.actividad_sol;

        ";
   
       $result = $this->db->query($sql);
   
       if ($result->num_rows() > 0) {
           return $result->result(); // Devolver todos los resultados
       } else {
           return 0; 
       }
   }
  // *********************** FIN ACTIVIDAD 6 ****************************


}//The class end
