<?php
class Bomberos  extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Bombero');

    }


    public function index()
	{
        // *********************** ACTIVIDAD 3 ****************************
        // Indicadores
        $data["List2020"] = $this->Bombero->getByTotalVisit20();
        $data["List2021"] = $this->Bombero->getByTotalVisit21();
        $data["List2022"] = $this->Bombero->getByTotalVisit22();
        $data["List2023"] = $this->Bombero->getByTotalVisit23();
        // Graficas
        $data["getByVisit2020"] = $this->Bombero->getByVisit20();
        $data["getByVisit2021"] = $this->Bombero->getByVisit21();
        $data["getByVisit2022"] = $this->Bombero->getByVisit22();
        $data["getByVisit2023"] = $this->Bombero->getByVisit23();

        // *********************** ACTIVIDAD 4 ****************************
        //codigo liz
                     $data["bomberosByNotificacion"] = $this->Bombero->getByTotalNotification("desc", 10);
                     $totalNotificaciones = $this->Bombero->getTotalNotificaciones();
                     $data["totalNotificaciones"] = $totalNotificaciones;
                     $data["totalNotificacionesTop10"] = $this->Bombero->getTotalNotificacionesTop10();
                         // Calcular el total de estados
                     $totalEstados = $this->Bombero->getTotalEstados();
                     // Pasar el total de estados a la vista
                     $data["totalEstados"] = $totalEstados;
                     $data["bomberosByState"] = $this->Bombero->getByTotalState("desc");
                     $data["bomberosByActividad"]=$this->Bombero->getByTotalActividad("desc",10);
                     // Calcular el total de actividades
                     $totalActividades = $this->Bombero->getTotalActividades();

                     // Pasar el total de actividades a la vista
                     $data["totalActividades"] = $totalActividades;

                     $data["bomberosByMeses"]=$this->Bombero->getByTotalMes("desc");
                     $data["totalSolicitudes"] = $this->Bombero->getTotalSolicitudes();

        // *********************** ACTIVIDAD 5 ****************************
        function getCantidadMensajesPorAnio() {
        $sql = "SELECT l.anio_lot, COUNT(m.codigo_men) AS cantidad_mensajes
                FROM lotaip l
                LEFT JOIN mensaje m ON YEAR(m.fecha_men) = l.anio_lot
                GROUP BY l.anio_lot;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalMensajes() {
    $sql = "SELECT COUNT(codigo_men) AS total_mensajes FROM mensaje";

    $result = $this->db->query($sql);

    if ($result->num_rows() > 0) {
        $total = $result->row()->total_mensajes;
        return $total;
    } else {
        return 0;
    }
}

    function getMensajesPorAnioYMes() {
        $sql = "SELECT YEAR(m.fecha_men) AS anio, MONTH(m.fecha_men) AS mes, COUNT(m.codigo_men) AS cantidad_mensajes
                FROM mensaje m
                GROUP BY YEAR(m.fecha_men), MONTH(m.fecha_men)
                ORDER BY anio, mes;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getCantidadArchivosPorTipo() {
        $sql = "SELECT 'Archivo' AS tipo, COUNT(*) AS cantidad_archivos
                FROM archivo
                UNION ALL
                SELECT 'Galeria' AS tipo, COUNT(*) AS cantidad_galerias
                FROM galeria;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalArchivosYGaleriasSumados() {
    $sql = "SELECT COUNT(*) AS total FROM archivo
            UNION ALL
            SELECT COUNT(*) FROM galeria";

    $result = $this->db->query($sql);

    if ($result->num_rows() > 0) {
        $rows = $result->result_array();
        $total = 0;

        foreach ($rows as $row) {
            $total += $row['total'];
        }

        return $total;
    } else {
        return 0;
    }
}


        // *********************** ACTIVIDAD 6 ****************************
        //Indicadores
        $data["totalNotifications"] = $this->Bombero->getByTotalNotiForKPI6();
        $data["totalSolicitudes"] = $this->Bombero->getByTotalSoliForKPI6();
        // getByTotalNotiForKPI7
        $data["totalSolicitudes_62"] = $this->Bombero->getByTotalSoliForKPI7();

        
        //Graficas
        $data["dataForKPI6"] = $this->Bombero->getDataForKPI6();
        $data["dataForKPI_62"] = $this->Bombero->getDataForKPI_62();
        $data["dataForKPI_63"] = $this->Bombero->getDataForKPI_63();
        $data["dataForKPI_64"] = $this->Bombero->getDataForKPI_64();


		$this->load->view('header');
		$this->load->view('bomberos/index', $data);
        $this->load->view('footer');
	}

}
?>
